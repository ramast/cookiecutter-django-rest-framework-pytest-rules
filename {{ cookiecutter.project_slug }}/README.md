{{cookiecutter.app_title}}
==========================

{{cookiecutter.short_description}}


Installation
------------

Create a virtual env then run 

```
pip install -r requirements/requirements.txt
pip install -r requirements/requirements.dev.txt
```

If code is added to a git repo, run the command `pre-commit install` to enable code quality checks before every commit.

If you are using pycharm, see `flake8-for-pycharm` section below.

For local enviroment create settings file `config/settings/local.py`
With following text inside

```python
from .base import *
DEBUG = True
```

Running Tests
-------------
* Activate python environment.
* export `DJANGO_SETTINGS_MODULE=config.settings.test`
* export `TEST_DATABASE_NAME=/tmp/testdb.sqlite3` (optional)
* run `pytest`

Chosen libraries / Frameworks
-----------------------------

### Django
https://docs.djangoproject.com/
> The main framework for this app

### Django Rest Framework
https://www.django-rest-framework.org/
> For creating API endpoints

### Rules
https://pypi.org/project/rules/
> For simplifying Django permission management

### DRF Spectacular
https://pypi.org/project/drf-spectacular/
> For auto generating OpenAPI3 schema for Django Rest Framework APIs.

### Django Model Helpers
https://pypi.org/project/django-model-helpers/
> Few useful django model helper functions

### Python DotEnv
https://pypi.org/project/python-dotenv/
> Reads key-value pairs from a .env file and set them as environment variables.

### pip-tools [DEV]
https://pypi.org/project/pip-tools/
> Instead of storing all project dependencies in `requirements.txt`,
> Only direct project dependences are added to `requirements.in` / `requirements.dev.in`
> then this tool auto generates `requirements.txt` / `requirements.dev.txt` accordingly.
> For this project, a script is provided ./requirements/rebuild_requirements.sh.
> Simply run it after making modifications to `.in` files to generate corresponding
> `.txt` fiels. Additionally if you use `pre-commit`, it will be executed automatically
> for you.

### pre-commit [DEV]
> Run different code quality apps before each commit.

### Black [DEV]
https://pypi.org/project/black/
> Auto format python code.

### isort [DEV]
> Auto sort python imports

### We Make Python Style Guide [DEV]
https://wemake-python-stylegui.de/en/latest/pages/usage/violations/index.html
> Evaluate quality of python code.

### Flake8 For PyCharm [Dev]
https://gitlab.com/ramast/flake8-for-pycharm
> PyCharm IDE only support pylint.
> This library adds support for flake8.
> See the documentation for how to use.

### pytest [DEV]
https://docs.pytest.org/en/7.1.x/
> Testing framework used by this project

### factory-boy [DEV]
> Speed up creation of Django model objects for test cases.

### ipdb [DEV]
> More fancy pdb command

### ipython [DEV]
> More fancy python shell

### dj-inmemorystorage [DEV]
> Files created during test cases are stored in memory instead of HD.
> This can significantly speedup running test cases.
----

This `Django`_ app was generated with `Cookiecutter`
* `Cookiecutter`: https://github.com/audreyr/cookiecutter
