from rest_framework import serializers

from .models import User


class CustomUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("username", "first_name", "last_name", "email")

    def update(self, instance, validated_data):
        # Username and email can't be altered
        validated_data.pop("email")
        validated_data.pop("username")
        return super().update(instance, validated_data)
