from rest_framework import viewsets
from rules.contrib.rest_framework import AutoPermissionViewSetMixin

from .models import User
from .serializers import CustomUserSerializer


class CustomUserViewset(AutoPermissionViewSetMixin, viewsets.ModelViewSet):
    queryset = User.objects.none()
    serializer_class = CustomUserSerializer

    def get_queryset(self):
        current_user = self.request.user
        if current_user.is_superuser:
            # Superuser can see everyone
            return User.objects.all()
        # If you are not a superuser, You can only see yourself
        return User.objects.filter(pk=current_user.pk)

    def get_object(self):
        if self.kwargs["pk"] == "me":
            # Make it so that calling /accounts/api/users/me return info about current user
            self.kwargs["pk"] = self.request.user.pk
        return super().get_object()
