from django.core.validators import RegexValidator
from django.utils.deconstruct import deconstructible


@deconstructible
class UsernameValidator(RegexValidator):
    regex = r"^[\w_]+$"
    message = "Enter a valid username. Username may contain only letters, numbers, and _ characters."
    flags = 0
