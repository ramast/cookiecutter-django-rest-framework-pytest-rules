import factory
from django.contrib.auth import get_user_model
from factory.django import DjangoModelFactory


class UserFactory(DjangoModelFactory):
    @factory.lazy_attribute
    def username(self):
        name_template = "user_{idx}"
        for idx in range(1, 21):
            # Find name that is not already taken
            name = name_template.format(idx=idx)
            if not get_user_model().objects.filter(username=name).extra():
                return name

    @factory.lazy_attribute
    def email(self):
        return "{username}@example.com".format(username=self.username)

    class Meta:
        model = "accounts.User"


class AdminUserFactory(UserFactory):
    is_staff = True
    is_superuser = True
