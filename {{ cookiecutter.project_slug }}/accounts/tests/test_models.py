import typing

import pytest

from . import factories

if typing.TYPE_CHECKING:
    from accounts.models import User


@pytest.mark.django_db(transaction=True)
@pytest.mark.parametrize("is_superuser", [True, False])
def test_user_str(is_superuser):
    user: "User" = factories.UserFactory(is_superuser=is_superuser, password="test")
    if is_superuser:
        assert str(user) == "[ADMIN] user_1"
    else:
        assert str(user) == "user_1"


def test_user_full_name_or_username():
    user_with_name: "User" = factories.UserFactory.build(username="abc", first_name="Ken", last_name="Adams")
    assert user_with_name.full_name_or_username == "Ken Adams"

    user_without_name: "User" = factories.UserFactory.build(username="abc")
    assert user_without_name.full_name_or_username == "abc"
