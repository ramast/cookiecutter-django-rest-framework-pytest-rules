import pytest
from rest_framework.test import APIClient

from . import factories

api_client = APIClient()


@pytest.mark.django_db
@pytest.mark.parametrize("is_superuser", [True, False])
def test_listing_users(is_superuser):
    user = factories.UserFactory(is_superuser=is_superuser, username="current_user")
    # Create another user
    factories.UserFactory()
    # Test listing users
    api_client.force_authenticate(user)
    response = api_client.get("/b/accounts/api/users/")
    assert response.status_code == 200, response
    if is_superuser:
        assert response.json() == {
            "count": 2,
            "next": None,
            "previous": None,
            "results": [
                {"email": "current_user@example.com", "first_name": "", "last_name": "", "username": "current_user"},
                {"email": "user_1@example.com", "first_name": "", "last_name": "", "username": "user_1"},
            ],
        }
    else:  # Normal user
        assert response.json() == {
            "count": 1,
            "next": None,
            "previous": None,
            "results": [
                {"email": "current_user@example.com", "first_name": "", "last_name": "", "username": "current_user"},
            ],
        }


@pytest.mark.django_db
def test_get_current_user_details():
    user = factories.UserFactory(username="current_user", first_name="a", last_name="b")
    # Create another user
    factories.UserFactory()
    # Test listing users
    api_client.force_authenticate(user)
    response = api_client.get("/b/accounts/api/users/me/")
    assert response.status_code == 200, response
    assert response.json() == {
        "email": "current_user@example.com",
        "first_name": "a",
        "last_name": "b",
        "username": "current_user",
    }


@pytest.mark.django_db
def test_create_user():
    # Test creating user
    user = factories.AdminUserFactory(username="admin_user", first_name="a", last_name="b")
    api_client.force_authenticate(user)
    response = api_client.post("/b/accounts/api/users/", {"username": "new_user", "email": "test@example.com"})
    assert response.status_code == 201, response
    assert response.json() == {"email": "test@example.com", "first_name": "", "last_name": "", "username": "new_user"}
    # Test creating user as normal user
    user = factories.UserFactory(username="current_user", first_name="a", last_name="b")
    api_client.force_authenticate(user)
    response = api_client.post("/b/accounts/api/users/", {"username": "new_user"})
    assert response.status_code == 403, response
