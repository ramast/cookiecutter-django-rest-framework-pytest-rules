import pytest

from .factories import UserFactory


@pytest.mark.django_db(transaction=True)
def test_user_add_perm():
    admin_user = UserFactory(is_superuser=True)
    normal_user = UserFactory(is_superuser=False)

    assert admin_user.has_perm("accounts.add_user")
    assert not normal_user.has_perm("accounts.add_user")


@pytest.mark.django_db(transaction=True)
def test_user_delete_perm():
    admin_user = UserFactory(is_superuser=True)
    normal_user = UserFactory(is_superuser=False)

    assert admin_user.has_perm("accounts.delete_user")
    assert not normal_user.has_perm("accounts.delete_user")


@pytest.mark.django_db(transaction=True)
def test_user_read_perm():
    admin_user = UserFactory(is_superuser=True)
    normal_user = UserFactory(is_superuser=False)

    # Admin user permissions
    assert admin_user.has_perm("accounts.view_user", normal_user)
    assert admin_user.has_perm("accounts.view_user", admin_user)

    # Normal user Permissions
    assert normal_user.has_perm("accounts.view_user", normal_user)
    assert not normal_user.has_perm("accounts.view_user", admin_user)


@pytest.mark.django_db(transaction=True)
def test_user_change_perm():
    admin_user = UserFactory(is_superuser=True)
    normal_user = UserFactory(is_superuser=False)

    # Admin user permissions
    assert admin_user.has_perm("accounts.change_user", normal_user)
    assert admin_user.has_perm("accounts.change_user", admin_user)

    # Normal user Permissions
    assert normal_user.has_perm("accounts.change_user", normal_user)
    assert not normal_user.has_perm("accounts.change_user", admin_user)
