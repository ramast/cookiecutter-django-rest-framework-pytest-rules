#!/bin/bash
export CUSTOM_COMPILE_COMMAND="$0"
if [ -z "$1" ]
then
    cd `dirname $0`
    # No arguments were passed, run for both files
    pip-compile --upgrade requirements.in --output-file requirements.txt
    pip-compile --upgrade requirements.dev.in --output-file requirements.dev.txt
else
    for file in $@
    do
        pip-compile $file --output-file "$(echo $file|sed 's:\.in$:.txt:' )"
    done
fi
