import multiprocessing
import os

service_port = 8001
errorlog = "logs/gunicorn_errors.txt"

settings = os.environ["DJANGO_SETTINGS_MODULE"].split(".")[2]
print(f"You are using {settings} settings")

daemon = True
capture_output = True
workers = multiprocessing.cpu_count() * 2 + 1
bind = [f"localhost:{service_port}"]
