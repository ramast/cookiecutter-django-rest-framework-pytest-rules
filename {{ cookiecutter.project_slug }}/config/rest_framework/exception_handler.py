from django.core import exceptions as django_exceptions
from rest_framework.exceptions import ValidationError
from rest_framework.views import exception_handler


def custom_exception_handler(exc, context):
    if isinstance(exc, django_exceptions.ValidationError):
        # Convert django validation error to restframework validation error
        if hasattr(exc, "message"):
            exc = ValidationError(exc.message)
        elif hasattr(exc, "message_dict"):
            exc = ValidationError(exc.message_dict)
        else:
            exc = ValidationError(exc.messages)

    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    if response.status_code == 403:
        if context["request"].method == "GET":
            response.data["detail"] = "Please login to view this section"
        else:
            response.data["detail"] = "Please login to perform this action"

    return response
